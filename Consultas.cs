﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using War.Modelo;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace War
{
    public class Consultas
    {
        List<Carta> lstCartas = new List<Carta>();

        //Metodo que obtiene la lista de cartas por jugador
        public List<Carta> ObtenerCartasXJugador(int id, List<Carta> lstCartas)
        {
            lstCartas = (from carta in lstCartas
                          where carta.IdJugador == id
                          select new Carta
                          {
                              IdJugador = (int)carta.IdJugador,
                              IdCarta = carta.IdCarta,
                              NoCarta = carta.NoCarta,
                              ValorCarta = carta.ValorCarta
                          }).ToList();

            return lstCartas;
        }

    }
}
