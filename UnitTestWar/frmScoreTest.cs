﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using War;

namespace UnitTestWar
{
    [TestClass]
    public class frmScoreTest
    {
        [TestMethod]
        public void ObtenerScore()
        {
            frmScore frm = new frmScore();

            List<War.Modelo.Score> lstScore = new List<War.Modelo.Score>();
            lstScore = frm.ObtenerScore();

            Assert.AreNotEqual(lstScore, null);
        }
    }
}
