﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using War;

namespace UnitTestWar
{
    [TestClass]
    public class frmWarTest
    {
        List<War.Modelo.Carta> lstOriginal = new List<War.Modelo.Carta>();

        [TestMethod]
        public void NextMove_CompararDosCartas_Jugador1GanoPartida()
        {
            War.frmWar frm = new War.frmWar();

            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 1,
                NoCarta = "A",
                ValorCarta = 14
            });
            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 2,
                NoCarta = "2",
                ValorCarta = 2
            });
            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 3,
                NoCarta = "5",
                ValorCarta = 5
            });


            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 4,
                NoCarta = "10",
                ValorCarta = 10
            });
            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 5,
                NoCarta = "K",
                ValorCarta = 13
            });
            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 6,
                NoCarta = "8",
                ValorCarta = 8
            });

            frm.NextMove();

            Assert.AreEqual(4, frm.lstCartas1.Count);
            Assert.AreEqual(frm.lstCartas1[2].IdCarta, 1);
            Assert.AreEqual(frm.lstCartas1[3].IdCarta, 4);

            Assert.AreEqual(2, frm.lstCartas2.Count);
            Assert.AreNotEqual(frm.lstCartas2[0].IdCarta, 4);
        }

        [TestMethod]
        public void NextMove_CompararDosCartas_Jugador2GanoPartida()
        {
            frmWar frm = new frmWar();

            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 1,
                NoCarta = "8",
                ValorCarta = 8
            });
            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 2,
                NoCarta = "2",
                ValorCarta = 2
            });
            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 3,
                NoCarta = "5",
                ValorCarta = 5
            });


            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 4,
                NoCarta = "10",
                ValorCarta = 10
            });
            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 5,
                NoCarta = "K",
                ValorCarta = 13
            });
            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 6,
                NoCarta = "8",
                ValorCarta = 8
            });

            frm.NextMove();

            Assert.AreEqual(4, frm.lstCartas2.Count);
            Assert.AreEqual(frm.lstCartas2[2].IdCarta, 4);
            Assert.AreEqual(frm.lstCartas2[3].IdCarta, 1);


            Assert.AreEqual(2, frm.lstCartas1.Count);
            Assert.AreNotEqual(frm.lstCartas1[0].IdCarta, 1);
        }

        [TestMethod]
        public void NextMove_CompararDosCartas_Jugador1GanoJuego()
        {
            frmWar frm = new frmWar();

            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 1,
                NoCarta = "A",
                ValorCarta = 14
            });
            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 2,
                NoCarta = "10",
                ValorCarta = 10
            });
            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 3,
                NoCarta = "8",
                ValorCarta = 8
            });


            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 4,
                NoCarta = "2",
                ValorCarta = 2
            });

            frm.NextMove();

            Assert.AreEqual(0, frm.lstCartas2.Count);
        }

        [TestMethod]
        public void NextMove_CompararDosCartas_Jugador2GanoJuego()
        {
            frmWar frm = new frmWar();

            frm.lstCartas1.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 1,
                NoCarta = "2",
                ValorCarta = 2
            });


            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 2,
                NoCarta = "10",
                ValorCarta = 10
            });
            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 1,
                IdCarta = 3,
                NoCarta = "8",
                ValorCarta = 8
            });

            frm.lstCartas2.Add(new War.Modelo.Carta
            {
                IdJugador = 2,
                IdCarta = 4,
                NoCarta = "A",
                ValorCarta = 13
            });

            frm.NextMove();

            Assert.AreEqual(0, frm.lstCartas1.Count);
        }

        [TestMethod]
        public void ObtenerCartas()
        {
            frmWar frm = new frmWar();
            lstOriginal = frm.IniciarCartas();

            Assert.AreEqual(lstOriginal.Count, 52);
        }

        [TestMethod]
        public void ObtenerCartasXJugador_JugadorCorrecto()
        {
            War.Consultas C = new Consultas();
            frmWar frm = new frmWar();

            int IdJugador = new Random().Next(2) + 1;

            List<War.Modelo.Carta> lst1 = new List<War.Modelo.Carta>();
            List<War.Modelo.Carta> lst = new List<War.Modelo.Carta>();
            lst = frm.IniciarCartas();

            lst1 = C.ObtenerCartasXJugador(IdJugador, lst);

            if (lst1.Count == 0)
            {
                Assert.Fail();
            }
            else
            {
                foreach (var item in lst1)
                {
                    Assert.AreEqual(IdJugador, item.IdJugador);
                }
            }
        }
    }
}
