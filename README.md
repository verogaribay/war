# WAR GAME

The ApiService and the database are in Azure Portal. The link is **https://warservice2.azurewebsites.net/** </br></br>
In the project WAR, exists the war project (the game) and the UnitTest project named UnitTestWar, which it contains some tests method. This project exists in the next repository **https://bitbucket.org/verogaribay/war** </br></br>
In the project RestFulWAR, exists the RestApi Service with one method to obtain the cards game for each player and another one to obtain the player’s score. This project exists in the next repository **https://bitbucket.org/verogaribay/restfulwar** </br></br>
The File **AspenCapital.sql**, contains all the tables, inserts and store procedure for the DataBase. The DataBase name’s is **BD_AspenCapital**. Remember the database already exists in the portal azure, you do not need execute the file. The file exists in the next repository **https://bitbucket.org/verogaribay/war/src/master/Modelo/**



## Steps to execute the game.

Launch the application **War.exe**, this is the game.

<img src="https://bitbucket.org/verogaribay/war/raw/37d6c26d772f845081a0bdd980157e12336d33d2/Images/exe.png" raw=true />
</br></br>

When you execute the **“exe”** maybe you will see this screen, please select “More Information/Mas información”.

<img src="https://bitbucket.org/verogaribay/war/raw/37d6c26d772f845081a0bdd980157e12336d33d2/Images/1.png" raw=true />
</br></br>

Then just press “Ejecutar de todas formas”.

<img src="https://bitbucket.org/verogaribay/war/raw/37d6c26d772f845081a0bdd980157e12336d33d2/Images/2.png" raw=true />
</br></br>



Now you can play war.

* The button “Iniciar juego/Start game” distribute the cards to each player.
* The button “Siguiente movimiento/Next move” check the players cards to decide wich card is bigger. A message box shows you the cards value of both players and who player won in every game.
* The button “Puntaje/Score” shows you a form with how many times won every player.

</br></br></br>
If I would have more time, I would like to added the card images to show them to the players.
