﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using War.Modelo;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace War
{
    public partial class frmWar : Form
    {
        Consultas objConsultas = new Consultas();
        List<Carta> lstCartasOriginal = new List<Carta>();
        public List<Carta> lstCartas1 = new List<Carta>();
        public List<Carta> lstCartas2 = new List<Carta>();

        public frmWar()
        {
            InitializeComponent();
        }

        #region EventosBotones
            private void btnIniciar_Click(object sender, EventArgs e)
            {
                lblCartas1.Text = "";
                lblCartas2.Text = "";
                label4.Text = "";
                label5.Text = "";
                label6.Text = "";
                label7.Text = "";

                lblCartas1.Visible = true;
                lblCartas2.Visible = true;

                lstCartasOriginal = this.IniciarCartas();

                lstCartas1 = objConsultas.ObtenerCartasXJugador(1, lstCartasOriginal);
                lstCartas2 = objConsultas.ObtenerCartasXJugador(2, lstCartasOriginal);

                foreach (var item in lstCartas1)
                {
                    lblCartas1.Text += item.NoCarta + ", ";
                }

                foreach (var item in lstCartas2)
                {
                    lblCartas2.Text += item.NoCarta + ", ";
                }

                btnCarta.Enabled = true;
                btnIniciar.Enabled = false;
            }

            private void btnCarta_Click(object sender, EventArgs e)
            {
                NextMove();
            }

            private void btnScore_Click(object sender, EventArgs e)
            {
                frmScore frm = new frmScore();
                frm.Show();
            }
        #endregion

        #region Acciones

        public void NextMove()
        {
            Carta obj1 = new Carta();
            Carta obj2 = new Carta();
            lblCartas1.Text = "";
            lblCartas2.Text = "";
            label6.Text = "";
            label7.Text = "";
            List<Carta> lst1 = new List<Carta>();
            List<Carta> lst2 = new List<Carta>();
            int idJugador = 0;

            if (lstCartas2.Count > 0 && lstCartas1.Count > 0)
            {
                if (lstCartas1[0].ValorCarta == lstCartas2[0].ValorCarta)
                {
                    MessageBox.Show("Jugador 1 [" + lstCartas1[0].NoCarta + "] VS [" + lstCartas2[0].NoCarta + "] Jugador 2. \n\nEl valor de la carta es igual.");

                    if (lstCartas1.Count == 2 || lstCartas2.Count == 2)
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            obj1 = new Carta();

                            obj1.IdCarta = lstCartas1[i].IdCarta;
                            obj1.IdJugador = lstCartas1[i].IdJugador;
                            obj1.NoCarta = lstCartas1[i].NoCarta;
                            obj1.ValorCarta = lstCartas1[i].ValorCarta;

                            lst1.Add(obj1);
                        }

                        for (int i = 0; i < 2; i++)
                        {
                            obj1 = new Carta();

                            obj1.IdCarta = lstCartas2[i].IdCarta;
                            obj1.IdJugador = lstCartas2[i].IdJugador;
                            obj1.NoCarta = lstCartas2[i].NoCarta;
                            obj1.ValorCarta = lstCartas2[i].ValorCarta;

                            lst2.Add(obj1);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            obj1 = new Carta();

                            obj1.IdCarta = lstCartas1[i].IdCarta;
                            obj1.IdJugador = lstCartas1[i].IdJugador;
                            obj1.NoCarta = lstCartas1[i].NoCarta;
                            obj1.ValorCarta = lstCartas1[i].ValorCarta;

                            lst1.Add(obj1);
                        }

                        for (int i = 0; i < 2; i++)
                        {
                            obj1 = new Carta();

                            obj1.IdCarta = lstCartas2[i].IdCarta;
                            obj1.IdJugador = lstCartas2[i].IdJugador;
                            obj1.NoCarta = lstCartas2[i].NoCarta;
                            obj1.ValorCarta = lstCartas2[i].ValorCarta;

                            lst2.Add(obj1);
                        }
                    }

                    foreach (var item in lst1)
                    {
                        label6.Text += item.NoCarta + ", ";
                    }

                    foreach (var item in lst2)
                    {
                        label7.Text += item.NoCarta + ", ";
                    }

                    if (lstCartas1.Count == 2 || lstCartas2.Count == 2)
                    {
                        if (lstCartas1[1].ValorCarta < lstCartas2[1].ValorCarta)
                        {
                            MessageBox.Show("Jugador 1 [" + lstCartas1[1].NoCarta + "] VS [" + lstCartas2[1].NoCarta + "] Jugador 2. \n\nLa carta del Jugador 2 es mayor.");

                            for (int i = 1; i <= 2; i++)
                            {
                                lstCartas1.RemoveAt(0);
                                lstCartas2.RemoveAt(0);
                            }

                            lstCartas2.AddRange(lst2);
                            lstCartas2.AddRange(lst1);
                        }
                        else if (lstCartas1[1].ValorCarta > lstCartas2[1].ValorCarta)
                        {
                            MessageBox.Show("Jugador 1 [" + lstCartas1[1].NoCarta + "] VS [" + lstCartas2[1].NoCarta + "] Jugador 2. \n\nLa carta del Jugador 1 es mayor.");

                            for (int i = 1; i <= 2; i++)
                            {
                                lstCartas1.RemoveAt(0);
                                lstCartas2.RemoveAt(0);
                            }
                            lstCartas1.AddRange(lst1);
                            lstCartas1.AddRange(lst2);
                        }

                        if (lstCartas1.Count == 0 || lstCartas2.Count == 0)
                        {
                            VerificarGanador();
                            return;
                        }
                    }
                    else
                    {
                        if (lstCartas1[1].ValorCarta < lstCartas2[1].ValorCarta)
                        {
                            MessageBox.Show("Jugador 1 [" + lstCartas1[1].NoCarta + "] VS [" + lstCartas2[1].NoCarta + "] Jugador 2. \n\nLa carta del Jugador 2 es mayor.");

                            for (int i = 1; i <= 2; i++)
                            {
                                lstCartas1.RemoveAt(0);
                                lstCartas2.RemoveAt(0);
                            }
                            lstCartas2.AddRange(lst2);
                            lstCartas2.AddRange(lst1);
                            
                        }
                        else if (lstCartas1[1].ValorCarta > lstCartas2[1].ValorCarta)
                        {
                            MessageBox.Show("Jugador 1 [" + lstCartas1[1].NoCarta + "] VS [" + lstCartas2[1].NoCarta + "] Jugador 2. \n\nLa carta del Jugador 1 es mayor.");

                            for (int i = 1; i <= 2; i++)
                            {
                                lstCartas1.RemoveAt(0);
                                lstCartas2.RemoveAt(0);
                            }
                            lstCartas1.AddRange(lst1);
                            lstCartas1.AddRange(lst2);
                        }


                        if (lstCartas1.Count == 0 || lstCartas2.Count == 0)
                        {
                            VerificarGanador();
                            return;
                        }
                    }
                }
                else if (lstCartas1[0].ValorCarta < lstCartas2[0].ValorCarta)
                {
                    MessageBox.Show("Jugador 1 [" + lstCartas1[0].NoCarta + "] VS [" + lstCartas2[0].NoCarta  + "] Jugador 2. \n\nLa carta del Jugador 2 es mayor.");
                    obj2.IdCarta = lstCartas2[0].IdCarta;
                    obj2.IdJugador = lstCartas2[0].IdJugador;
                    obj2.NoCarta = lstCartas2[0].NoCarta;
                    obj2.ValorCarta = lstCartas2[0].ValorCarta;

                    obj1.IdCarta = lstCartas1[0].IdCarta;
                    obj1.IdJugador = lstCartas1[0].IdJugador;
                    obj1.NoCarta = lstCartas1[0].NoCarta;
                    obj1.ValorCarta = lstCartas1[0].ValorCarta;

                    lstCartas1.RemoveAt(0);
                    lstCartas2.RemoveAt(0);
                    lstCartas2.Add(obj2);
                    lstCartas2.Add(obj1);

                    if (lstCartas1.Count == 0 || lstCartas2.Count == 0)
                    {
                        VerificarGanador();
                        return;
                    }
                }
                else if (lstCartas1[0].ValorCarta > lstCartas2[0].ValorCarta)
                {
                    MessageBox.Show("Jugador 1 [" + lstCartas1[0].NoCarta + "] VS [" + lstCartas2[0].NoCarta + "] Jugador 2. \n\nLa carta del Jugador 1 es mayor.");
                    obj1.IdCarta = lstCartas1[0].IdCarta;
                    obj1.IdJugador = lstCartas1[0].IdJugador;
                    obj1.NoCarta = lstCartas1[0].NoCarta;
                    obj1.ValorCarta = lstCartas1[0].ValorCarta;

                    obj2.IdCarta = lstCartas2[0].IdCarta;
                    obj2.IdJugador = lstCartas2[0].IdJugador;
                    obj2.NoCarta = lstCartas2[0].NoCarta;
                    obj2.ValorCarta = lstCartas2[0].ValorCarta;

                    lstCartas1.RemoveAt(0);
                    lstCartas2.RemoveAt(0);
                    lstCartas1.Add(obj1);
                    lstCartas1.Add(obj2);

                    if (lstCartas1.Count == 0 || lstCartas2.Count == 0)
                    {
                        VerificarGanador();
                        return;
                    }
                }

                foreach (var item in lstCartas1)
                {
                    lblCartas1.Text += item.NoCarta + ", ";
                }

                foreach (var item in lstCartas2)
                {
                    lblCartas2.Text += item.NoCarta + ", ";
                }
            }
            else
            {
                if (lstCartas1.Count == 0)
                {
                    MessageBox.Show("No existen cartas suficientes para el jugador 1.");
                    MessageBox.Show("El GANADOR es el Jugador 2.");
                }
                else if (lstCartas2.Count == 0)
                {
                    MessageBox.Show("No existen cartas suficientes para el jugador 2.");
                    MessageBox.Show("El GANADOR es el Jugador 1.");
                }

                if (lstCartas1.Count > 0)
                {
                    idJugador = lstCartas1[0].IdJugador;
                }
                else if (lstCartas2.Count > 0)
                {
                    idJugador = lstCartas2[0].IdJugador;
                }

                InsertarScore(idJugador);
            }
        }
        #endregion

        #region Accion
        public void VerificarGanador()
        {
            int idJugador = 0;

            if (lstCartas1.Count == 0)
            {
                MessageBox.Show("El GANADOR es el Jugador 2.");
                idJugador = lstCartas2[0].IdJugador;
                InsertarScore(idJugador);

                btnIniciar.Enabled = true;
                btnCarta.Enabled = false;
            }
            else if (lstCartas2.Count == 0)
            {
                MessageBox.Show("El GANADOR es el Jugador 1.");
                idJugador = lstCartas1[0].IdJugador;
                InsertarScore(idJugador);

                btnIniciar.Enabled = true;
                btnCarta.Enabled = false;
            }
        }
        #endregion


        #region OperacionesServidor
            public List<Carta> IniciarCartas()
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                var url = $"https://warservice2.azurewebsites.net/Api/Cartas";
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Accept = "application/json";

                try
                    {
                        using (WebResponse response = request.GetResponse())
                        {
                            using (Stream strReader = response.GetResponseStream())
                            {
                                if (strReader == null) return null;
                                using (StreamReader objReader = new StreamReader(strReader))
                                {
                                    string responseBody = objReader.ReadToEnd();

                                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();

                                    lstCartasOriginal = jsonSerializer.Deserialize<List<Carta>>(responseBody);
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        MessageBox.Show("Upss! Ocurrio un error al conectar con el servidor.");
                    }
                return lstCartasOriginal;
            }

            public void InsertarScore(int Idjugador)
            {
                Carta p = new Carta { IdJugador = Idjugador, IdCarta = 0, NoCarta = "0", ValorCarta = 0 };
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                var url = $"https://warservice2.azurewebsites.net/Api/Score";
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                var json = new JavaScriptSerializer().Serialize(p);
                request.ContentType = "application/json";
                request.Accept = "application/json";
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                try
                {
                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream strReader = response.GetResponseStream())
                        {
                            if (strReader == null) return;
                            using (StreamReader objReader = new StreamReader(strReader))
                            {
                                string responseBody = objReader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (WebException ex)
                {
                    MessageBox.Show("Upss! Ocurrio un error al insertar los datos.");
                }

            }
        #endregion
    }
}
