﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using War.Modelo;
using System.Web.Script.Serialization;

namespace War
{
    public partial class frmScore : Form
    {
        List<Score> lstScore = new List<Score>();

        public frmScore()
        {
            InitializeComponent();
            lstScore = ObtenerScore();
            dgvScore.DataSource = lstScore;
        }

        public List<Score> ObtenerScore()
        {
            List<Score> lst = new List<Score>();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            var url = $"https://warservice2.azurewebsites.net/Api/Score";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        if (strReader == null) return null;
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();                            
                            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                            lst = jsonSerializer.Deserialize<List<Score>>(responseBody);

                            if (lst.Count == 0)
                            {
                                MessageBox.Show("No se encontro información.");
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show("Upss! Ocurrio un error al conectar con el servidor.");
            }

            return lst;
        }
            
    }
}
