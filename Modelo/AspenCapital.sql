USE [master]
GO

IF NOT EXISTS (SELECT * FROM sysdatabases WHERE (name = 'BD_AspenCapital')) 
BEGIN
	CREATE DATABASE BD_AspenCapital
	PRINT 'CREANDO OBJETOS DE BASE DE DATOS...'
END
ELSE
BEGIN
	DROP DATABASE BD_AspenCapital
END
GO


USE BD_AspenCapital
GO
/*
Borramos las llaves foraneas y en caso de que las tablas existan las borramos para crearlas de nuevo
*/

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'FK_IdCategoria')
BEGIN
	ALTER TABLE tbCartas DROP CONSTRAINT FK_IdCategoria
	DROP TABLE tbCategoria
END
GO
IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'tbJugador')
BEGIN
	DROP TABLE tbJugador
END
GO

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'tbCartas')
BEGIN
	DROP TABLE tbCartas
END
GO

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'tbScore')
BEGIN
	DROP TABLE tbScore
END
GO

/*
Creamos las tablas siempre y cuando no existan
*/
IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'tbCategoria')
BEGIN
	CREATE TABLE tbCategoria(
		IdCategoria int identity(1, 1),
		vchCategoria varchar(8) not null,
		Simbolo varchar (1),
		CONSTRAINT PK_IdCategoria PRIMARY KEY (IdCategoria)
)
END
GO

IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'tbCartas')
BEGIN
	CREATE TABLE tbCartas(
		idCarta int identity(1,1),
		ValorCarta int not null,
		NoCarta varchar(2) not null,
		IdCategoria int not null,
		CONSTRAINT PK_idCarta PRIMARY KEY (idCarta),
		CONSTRAINT FK_IdCategoria FOREIGN KEY (IdCategoria) REFERENCES tbCategoria
	)
END
GO

IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'tbJugador')
BEGIN
	CREATE TABLE tbJugador (
		id int identity (1,1),
		idJugador int,
		idCarta int,
		CONSTRAINT PK_id PRIMARY KEY (id),
		CONSTRAINT FK_JidCarta FOREIGN KEY (idCarta) REFERENCES tbCartas
	)
END
GO

IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'tbScore')
BEGIN
	CREATE TABLE tbScore (
		idScore int identity (1,1),
		idJugador int,
		dtFecha DATETIME DEFAULT GETDATE() ,
		CONSTRAINT PK_idScore PRIMARY KEY (idScore)
	)
END
GO


/*
	Insertamos las categorias
*/
INSERT INTO tbCategoria (vchCategoria, Simbolo)
VALUES ('Corazon', '?')
GO

INSERT INTO tbCategoria (vchCategoria, Simbolo)
VALUES ('Diamante', '?')
GO

INSERT INTO tbCategoria (vchCategoria, Simbolo)
VALUES ('Trebol', '?')
GO

INSERT INTO tbCategoria (vchCategoria, Simbolo)
VALUES ('Pica', '?')
GO


/*
	INSERTAMOS LAS 52 CARTAS
*/
INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT 'A', 14, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT 'K', 13, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT 'Q', 12, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT 'J', 11, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '10', 10, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '9', 9, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '8', 8, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '7', 7, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '6', 6, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '5', 5, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '4', 4, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '3', 3, C.IdCategoria
FROM tbCategoria C
GO

INSERT INTO tbCartas (NoCarta, ValorCarta, IdCategoria)
SELECT '2', 2, C.IdCategoria
FROM tbCategoria C
GO




IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'ObtenerCartas')
BEGIN
	DROP PROCEDURE ObtenerCartas
END
GO
CREATE PROCEDURE ObtenerCartas
/*
Procedimiento que obtiene las cartas para repartirlas a los dos jugadores
*/
AS
DELETE FROM tbJugador

INSERT INTO tbJugador (idCarta)
SELECT C.idCarta
FROM tbCartas C
ORDER BY NEWID()

UPDATE TOP (26) tbJugador
SET idJugador = 1

UPDATE tbJugador
SET idJugador = 2
WHERE IdJugador IS NULL

SELECT J.idJugador, C.idCarta, C.NoCarta, C.ValorCarta, C.IdCategoria, Ct.Simbolo
FROM tbJugador J, tbCartas C, tbCategoria Ct
WHERE J.idCarta = C. idCarta
AND C.IdCategoria = Ct.IdCategoria
ORDER BY idJugador, NEWID()

GO

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = 'ObtenerScore')
BEGIN
	DROP PROCEDURE ObtenerScore
END
GO
CREATE PROCEDURE ObtenerScore
/*
Procedimiento que obtiene el score de los jugadores
*/
AS
SELECT idJugador, count (*) AS WINS, max(dtFecha) AS dtFecha FROM tbScore
GROUP BY idJugador
GO


