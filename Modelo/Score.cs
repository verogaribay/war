﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace War.Modelo
{
    //Clase para el score de los jugadores
    public class Score
    {
        [DisplayName("Juegos ganados")]
        public int Wins { get; set; }

        [DisplayName("Jugador")]
        public int IdJugador { get; set; }

        [DisplayName("Ultimo juego ganado")]
        public string dtFecha { get; set; }
    }
}
