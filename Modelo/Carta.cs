﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace War.Modelo
{
    //Clase para las cartas de los jugadores
    public class Carta
    {
        public int IdJugador { get; set; }
        public int IdCarta { get; set; }
        public string NoCarta { get; set; }
        public int ValorCarta { get; set; }
    }
}
